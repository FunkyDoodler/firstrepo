﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuLevelManager : MonoBehaviour 
{

    [SerializeField]
    private bool displayAll = false;
    [SerializeField]
    private StartMenuLevel[] levels;

    private PlayerData pd;

	// Use this for initialization
	void Start () 
	{
        GetComponents();
        DisplayPlayableLevels();

        if (displayAll)
            pd.SaveCurProgress(levels.Length);
    }

    void GetComponents()
    {
        //getcomponents
        pd = GameManager.instance.GetPlayerData();
    }

    public void DisplayPlayableLevels()
    {

        foreach (var level in levels)
        {
            if (pd.GetCurProgress() >= level.GetLevelInd())
            {
                level.SetLevelPlayable(true);
            }
            else
            {
                level.SetLevelPlayable(false);
            }

        }
    }
	
}
