﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public enum JumpStyle { Consistent, Additive }

    //Movement
    [SerializeField]
    private float speed = 10;
    [SerializeField]
    private bool enableRun = true;
    [SerializeField]
    private float runSpeed = 15;
    private BoolWrapper run = new BoolWrapper(false);
    private bool running;
    [SerializeField]
    private bool enableCrouch = true;
    [SerializeField]
    private float crouchSpeed = 5;
    [SerializeField]
    private float crouchSpeedTime = 1;
    private BoolWrapper crouch = new BoolWrapper(false);
    private bool crouching;
    [SerializeField]
    private bool enableClimbing = true;
    [SerializeField]
    private float climbSpeed = 6;
    public bool climbing;
    private bool onLadder;
    private float curSpeed;
    [SerializeField]
    private float inAirSpeedTime = 1;

    //Jumping
    [SerializeField]
    private JumpStyle jumpStyle = JumpStyle.Additive;
    [SerializeField]
    private float jumpPower = 6;
    private bool jump;
    private bool doubleJumping;
    [SerializeField]
    private bool enableDoubleJump;
    private bool doubleJumpActive;
    [SerializeField]
    private bool enableJumpClimbing = true;
    private BoolWrapper jumpClimbing = new BoolWrapper(false);
    [SerializeField]
    private float climbJumpTime = 1;
    [SerializeField]
    private bool faceRightAtStart = true;
    [SerializeField]
    private float gravityMultiplier;
    [SerializeField]
    private float lowJumpMultiplier;

    //Ground Detection
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    private Vector2 groundBoxSize = Vector2.one;
    [SerializeField]
    private Vector2 groundBoxCenter = Vector2.zero;
    private bool grounded;
    private Collider2D[] groundHits;
    private bool onPlatform;
    private GameObject currentGroundGO;

    //Wall Detection
    [SerializeField]
    private bool enableWallJump = true;
    [SerializeField]
    private LayerMask wallMask;
    [SerializeField]
    private Vector2 leftDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 leftDetectCenter = Vector2.left;
    [SerializeField]
    private Vector2 rightDetectSize = Vector2.one;
    [SerializeField]
    private Vector2 rightDetectCenter = Vector2.right;
    [SerializeField]
    private float wallBounceForceX = 5;
    [SerializeField]
    private float wallBounceForceY = 5;
    private bool wallHitLeft;
    private bool wallHitRight;
    [SerializeField]
    private bool disableMovementOnWallJump = true;
    [SerializeField]
    private float disableTime = 1;

    //movement inputs
    private float inputHor;
    private float inputVer;
    private Vector2 move;

    //comps
    private Player pl;
    private CapsuleCollider2D conCollider;
    private Rigidbody2D rb;
    private PlayerAnimations anim;

    private bool facingRight = true;

    private Vector2 startColOffset;
    private Vector2 startColSize;

    private BoolWrapper disableInput = new BoolWrapper(false);

    private float lastYPos;
    private float curYPos;

    private bool bouncing;

    private void Start()
    {
        InitializeController();
    }

    private void Update()
    {
        GetInputs();
        CheckDirection();
        CheckSpeed();
        CheckHeightVelocity();
        CheckGrounded();
        CheckWallHits();
        CheckClimbing();
        CheckBouncing();
        CheckKillHeight();
        SyncAnimations();
    }

    private void FixedUpdate()
    {
        Move();
    }

    void InitializeController()
    {
        //set start speed
        curSpeed = speed;
        //set Direction
        if (!faceRightAtStart)
            FlipController();

        //get collider
        conCollider = GetComponent<CapsuleCollider2D>();
        startColSize = conCollider.size;
        startColOffset = conCollider.offset;

        //get comps
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<PlayerAnimations>();
        pl = GetComponent<Player>();
    }

    void GetInputs()
    {
        if (pl.IsDead())
            return;

        if (disableInput.Value)
        {
            inputHor = 0;
            inputVer = 0;
            return;
        }
            
        //movement axis input
        inputHor = Input.GetAxisRaw("Horizontal");
        inputVer = Input.GetAxisRaw("Vertical");
        move = new Vector2(inputHor, inputVer).normalized;

        //jump input
        jump = Input.GetButtonDown("Jump");
        if (jump)
            Jump();

        //run input
        if (Input.GetButton("Run") || Input.GetAxisRaw("XboxTriggerLeft") > 0)
            run.Value = true;
        else
            run.Value = false;

        //crouch input
        if (move.y < -0.69f)
            crouch.Value = true;
        else
            crouch.Value = false;
    }

    void Move()
    {
        if (pl.IsDead())
            return;

        if (disableInput.Value)
            return;

        //move controller
        if (climbing)
        {
            Climb();
        }
        else
        {
            Vector2 curMovement = transform.right * inputHor * curSpeed * Time.deltaTime;
            transform.Translate(curMovement);
        }

    }

    void Jump()
    {
        if (!grounded && !doubleJumping && enableDoubleJump)
        {
            //enable double jumping until grounded/climbing
            StartCoroutine(StartJumpSwitch());
            doubleJumpActive = true;
        }
            
        if (grounded || doubleJumpActive || wallHitLeft || wallHitRight || climbing)
        {
            Vector2 force = Vector2.up * jumpPower;
            if (!grounded)
            {
                if (wallHitLeft || wallHitRight)
                {
                    if (!enableWallJump)
                        return;

                    if (wallHitLeft)
                    {
                        if (!facingRight)
                            force = new Vector2(wallBounceForceX, jumpPower + wallBounceForceY);
                        else
                            force = Vector2.zero;
                    }
                    else if (wallHitRight)
                    {
                        if (facingRight)
                            force = new Vector2(-wallBounceForceX, jumpPower + wallBounceForceY);
                        else
                            force = Vector2.zero;
                    }

                    if (disableMovementOnWallJump)
                        StartCoroutine(StartSpeedFade(0, curSpeed, disableTime, new BoolWrapper(!grounded)));
                }
                else if (climbing && enableJumpClimbing)
                    StartCoroutine(StartBoolTimer(jumpClimbing, climbJumpTime));
            }
            else if (climbing && enableJumpClimbing)
                StartCoroutine(StartBoolTimer(jumpClimbing, climbJumpTime));

            //jump player
            rb.velocity = force;

            //set double jump
            if (enableDoubleJump)
            {
                if (doubleJumpActive)
                    doubleJumpActive = false;
            }

        }

    }

    void CheckGrounded()
    {

        curYPos = Mathf.Round(transform.position.y * 100) / 100;

        if (curYPos != lastYPos)
        {
            //ground detect
            groundHits = Physics2D.OverlapBoxAll((Vector2)transform.position + groundBoxCenter, groundBoxSize, 0, groundMask);
            if (groundHits.Length > 0)
            {
                currentGroundGO = groundHits[0].gameObject;
                grounded = true;

                if (currentGroundGO.layer == LayerMask.NameToLayer("Platform") && !onPlatform)
                {
                    StickToPlatform(currentGroundGO.transform, true);
                }
            }
            else if (onPlatform)
            {
                StickToPlatform(null, false);
            }
            else
                grounded = false;
        }

        lastYPos = Mathf.Round(transform.position.y * 100) / 100;

    }

    void CheckBouncing()
    {
        if (grounded && bouncing || jump)
            bouncing = false;
    }

    void StickToPlatform(Transform _platform, bool _stick)
    {
        onPlatform = _stick;
        transform.SetParent(_platform);
    }

    void CheckClimbing()
    {
        if (!enableClimbing)
            return;

        if (onLadder && inputVer > 0 && !jumpClimbing.Value)
        {
            climbing = true;

            if (!facingRight)
                FlipController();
        }
        if (!onLadder && climbing)
        {
            StopClimbing();
        }
        if (jumpClimbing.Value && climbing)
            StopClimbing();
    }

    void CheckWallHits()
    {
        //left detect        
        wallHitLeft = Physics2D.OverlapBox((Vector2)transform.position + leftDetectCenter, leftDetectSize, 0, wallMask);
        //right detect
        wallHitRight = Physics2D.OverlapBox((Vector2)transform.position + rightDetectCenter, rightDetectSize, 0, wallMask);
    }

    void CheckHeightVelocity()
    {
        if (climbing || jumpStyle == JumpStyle.Consistent || bouncing)
            return;

        if (rb.velocity.y < 0)
            AddGravity(gravityMultiplier);
        if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            AddGravity(lowJumpMultiplier);
    }

    void AddGravity(float _multiplier)
    {
        rb.velocity += Vector2.up * Physics2D.gravity.y * _multiplier * Time.deltaTime;
    }

    void CheckDirection()
    {
        if (climbing)
            return;

        //flip controller based on input left or right
        if (inputHor < 0 && facingRight || inputHor > 0 && !facingRight)
        {
            FlipController();
        }
    }

    void CheckSpeed()
    {
        if (run.Value && !crouching && !crouch.Value)
        {
            Run();
        }
        else if (climbing)
            curSpeed = climbSpeed;
        else if (crouch.Value)
        {
            Crouch();
        }
        else if (grounded && curSpeed != speed)
            ResetSpeed();

        //Stop player movement if they are running into walls
        if (grounded)
        {
            if (wallHitRight && inputHor > 0)
                inputHor = 0;
            if (wallHitLeft && inputHor < 0)
                inputHor = 0;
        }
    }

    void CheckKillHeight()
    {
        if (transform.position.y < GameManager.instance.GetKillHeight())
        {
            pl.KillPlayer();
        }
    }

    void Climb()
    {
        if (inputVer == 0)//freeze y position so there is no slow fall
            rb.constraints = (RigidbodyConstraints2D)6;
        else//unfreeze y position but reset velocity
        {
            rb.velocity = Vector2.zero;
            rb.constraints = (RigidbodyConstraints2D)4;
        }

        transform.Translate(move * curSpeed * Time.deltaTime);
    }

    void StopClimbing()
    {
        rb.constraints = (RigidbodyConstraints2D)4;
        climbing = false;
    }

    void Crouch()
    {
        if (crouching || !enableCrouch || climbing)
            return;

        crouching = true;
        CrouchSize(true);

        if (curSpeed > crouchSpeed)
            StartCoroutine(StartSpeedFade(curSpeed, crouchSpeed, crouchSpeedTime, crouch));
        else
            curSpeed = crouchSpeed;
    }

    void Run()
    {
        if (!enableRun)
            return;

        running = true;
        if (grounded)
            curSpeed = runSpeed;
        else
            StartCoroutine(StartSpeedFade(curSpeed, runSpeed, inAirSpeedTime, run));
    }

    void ResetSpeed()
    {
        curSpeed = speed;
        running = false;
        if (crouching)
        {
            CrouchSize(false);
            crouching = false;
        }

    }

    IEnumerator StartSpeedFade(float _startSpeed, float _endSpeed, float _time, BoolWrapper _condition)
    {
        float timer = 0;
        float perc = 0;
        while (perc < 1 && _condition.Value)
        {
            timer += Time.deltaTime;
            if (timer > _time)
                timer = _time;
            perc = timer / _time;
            curSpeed = Mathf.Lerp(_startSpeed, _endSpeed, perc);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator StartBoolTimer(BoolWrapper _bool, float _time)
    {
        float timer = 0;
        float perc = 0;
        _bool.Value = true;
        while (perc < 1)
        {
            timer += Time.deltaTime;
            if (timer > _time)
                timer = _time;
            perc = timer / _time;
            yield return new WaitForEndOfFrame();
        }
        _bool.Value = false;
    }

    IEnumerator StartJumpSwitch()
    {
        //anim switch
        if (anim)
            anim.BoolSwitch(anim.doubleJumpSwitch, true, 1);

        //possible to do a jump out of the air if an input jump was not the cause for being not grounded.
        doubleJumping = true;
        while (!grounded && !climbing)
            yield return new WaitForEndOfFrame();
        doubleJumping = false;
    }

    void CrouchSize(bool _crouch)
    {
        if (_crouch)
        {
            crouching = true;
            conCollider.offset = new Vector2(conCollider.offset.x, startColOffset.y / 2);
            conCollider.size = new Vector2(conCollider.size.x, startColSize.y / 2);
        }
        else
        {
            crouching = false;
            conCollider.offset = startColOffset;
            conCollider.size = startColSize;
        }

    }

    void FlipController()
    {
        transform.Rotate(0, 180, 0);
        facingRight = !facingRight;
    }

    public void Bounce(Vector2 _direction, float _force)
    {
        bouncing = true;
        rb.velocity = _direction * _force;
    }

    public void StunDisableMovement(float _time)
    {
        StartCoroutine(StartBoolTimer(disableInput,_time));
    }

    public void DisableInput(bool _disable)
    {
        disableInput.Value = _disable;
    }

    public bool IsOnLadder
    {
        get { return onLadder; }
        set
        {
            onLadder = value;
        }
    }

    public bool IsGrounded()
    {
        return grounded;
    }

    void SyncAnimations()
    {
        if (!anim)
            return;

        anim.grounded = grounded;
        anim.running = running;
        anim.crouching = crouching;
        anim.climbing = climbing;
        anim.jump = jump;
        anim.wallHitLeft = wallHitLeft;
        anim.wallHitRight = wallHitRight;
        anim.inputHor = inputHor;
        anim.inputVer = inputVer;
    }

}
