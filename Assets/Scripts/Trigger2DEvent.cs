﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Internal;

[RequireComponent(typeof(Collider2D))]
public class Trigger2DEvent : MonoBehaviour
{

    public int mask = 0;
    public string[] maskOptions = new string[] { "Trigger2DEnter", "Trigger2DExit", "Trigger2DStay" };

    public string triggerTag = "Untagged";
    public UnityEvent trigger2DEnterEvents;
    public UnityEvent trigger2DExitEvents;
    public UnityEvent trigger2DStayEvents;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (mask == 1 | mask == 3 | mask == 5 | mask == -1)
        {
            if (col.tag == triggerTag)
            {
                trigger2DEnterEvents.Invoke();
            }
        }
        else
            return;    
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (mask == 2 | mask == 3 | mask == 6 | mask == -1)
        {
            if (col.tag == triggerTag)
            {
                trigger2DExitEvents.Invoke();
            }
        }
        else
            return;     
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (mask == 4 | mask == 5 | mask == 6 | mask == -1)
        {
            if (col.tag == triggerTag)
            {
                trigger2DStayEvents.Invoke();
            }
        }
        else
            return;
    }

}
