﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    [SerializeField]
    private float lifeTime = 10;
    private float lifeTimer;

    private float speed;
    private int damage;

    // Update is called once per frame
    void Update()
    {

        MoveForward();
        KillTimer();
    }

    void MoveForward()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    void KillTimer()
    {
        lifeTimer += Time.deltaTime;
        if (lifeTimer > lifeTime)
        {
            KillProjectile();
        }
    }

    void KillProjectile()
    {
        Destroy(this.gameObject);
    }

    public void SetSpeed(float _speed)
    {
        speed = _speed;
    }

    public void SetDamage(int _damage)
    {
        damage = _damage;
    }

    void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.tag == "Player")
        {
            DamagePlayer(_col.GetComponent<Player>());
        }
    }

    void DamagePlayer(Player _player)
    {
        _player.DamageHp(damage);
        KillProjectile();
    }
}
