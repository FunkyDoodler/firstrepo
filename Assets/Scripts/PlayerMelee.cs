﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMelee : MonoBehaviour 
{

    [SerializeField]
    private int damage;
    [SerializeField]
    private float hitDelay = 0.05f;
    private float timer;
    [SerializeField]
    private Transform meleePos;
    [SerializeField]
    private float hitRadius = 0.3f;
    [SerializeField]
    private LayerMask hitMask;
    [SerializeField]
    private int numMeleeAttacks = 3;

    private bool attack;
    private bool attacking;

    private MenuManager mm;
    private PlayerAnimations anim;
    private Player player;

    private PlayerSoundFX soundFX;

    private GameObject curSkin;

	// Use this for initialization
	void Start () 
	{
        //getcomp
        mm = GameManager.instance.GetMenuManager();
        anim = GetComponent<PlayerAnimations>();
        player = GetComponent<Player>();

        //wait 1 frame for skin to be chosen
        StartCoroutine(GetCurSkin());
	}

    IEnumerator GetCurSkin()
    {
        yield return new WaitForEndOfFrame();
        curSkin = player.GetCurSkin();

        //get sound FX
        soundFX = curSkin.GetComponent<PlayerSoundFX>();
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (mm.isPaused())
            return;

        GetInputs();
        CheckAttacking();
	}

    void GetInputs()
    {
        attack = Input.GetButtonDown("Fire1");

        if (attack)
        {
            if (!attacking)
            {
                attacking = true;
                //set random melee animation
                anim.SetMeleeNum(Random.Range(0,numMeleeAttacks));

                //playSound
                soundFX.PlayAttackSound();
            }
                
        }

        //animations
        //anim.attack = attack;

    }

    void CheckAttacking()
    {
        if (attacking)
        {
            timer += Time.deltaTime;
            if (timer > hitDelay)
            {
                CreateHitZone();
                attacking = false;
                timer = 0;
            }
        }
    }

    void CreateHitZone()
    {
        //grabs all colliders in the layer mask within that circle
        Collider2D[] cols = Physics2D.OverlapCircleAll(meleePos.position, hitRadius, hitMask);

        //cycle through the colliders..grabbing the enemies
        foreach (var col in cols)
        {
            //grab enemy health component
            Enemy enemy = col.transform.root.GetComponentInChildren<Enemy>();

            if (enemy) // only applies damage if component is found
                //damage enemy
                DamageEnemy(enemy);
        }
    }

    void DamageEnemy(Enemy _enemy)
    {
        _enemy.DamageHp(damage);
    }
}
