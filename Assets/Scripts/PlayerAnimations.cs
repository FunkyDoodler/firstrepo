﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour 
{

    //movement
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private string animInputHor = "horizontal";
    [SerializeField]
    private string animInputVer = "vertical";
    [SerializeField]
    private string animRun = "run";
    [SerializeField]
    private string animCrouch = "crouch";
    [SerializeField]
    private string animClimbing = "climbing";
    [SerializeField]
    private string animGrounded = "grounded";

    //jumping
    [SerializeField]
    private string animJump = "jump";
    [SerializeField]
    private string animDoubleJump = "doubleJump";
    [SerializeField]
    private string animWallHitLeft = "wallHitLeft";
    [SerializeField]
    private string animWallHitRight = "wallHitRight";

    //health
    [SerializeField]
    private string animHurt = "hurt";
    [SerializeField]
    private string animDead = "dead";

    public bool jump;
    public BoolWrapper doubleJumpSwitch = new BoolWrapper(false);
    public bool grounded;
    public bool running;
    public bool climbing;
    public bool right;
    public BoolWrapper hurt = new BoolWrapper(false);
    private bool dead;
    public bool crouching;
    public bool wallHitLeft;
    public bool wallHitRight;

    public float inputHor;
    public float inputVer;

    [SerializeField]
    private int maxDeathAnims = 1;
    [SerializeField]
    private int maxHurtAnims = 1;

	public void SetAnimationController(Animator _controller)
    {
        anim = _controller;
    }
	
	// Update is called once per frame
	void Update () 
	{
        SyncAnimations();
    }

    void SyncAnimations()
    {
        if (!anim)
            return;

        //movement
        anim.SetFloat(animInputHor, Mathf.Abs(inputHor));
        anim.SetFloat(animInputVer, (inputVer));
        anim.SetBool(animGrounded, grounded);
        anim.SetBool(animRun, running);
        anim.SetBool(animCrouch, crouching);
        anim.SetBool(animClimbing, climbing);

        //jumping
        anim.SetBool(animJump, jump);
        anim.SetBool(animDoubleJump, doubleJumpSwitch.Value);
        anim.SetBool(animWallHitLeft, wallHitLeft);
        anim.SetBool(animWallHitRight, wallHitRight);

        //health
        anim.SetBool(animHurt, hurt.Value);
        anim.SetBool(animDead, dead);
    }

    public void BoolSwitch(BoolWrapper _boolWrapper, bool _switchOn, int _frameCount)
    {
        StartCoroutine(StartBoolSwitch(_boolWrapper, _switchOn, _frameCount));
    }

    IEnumerator StartBoolSwitch(BoolWrapper _boolWrapper, bool _switchOn, int _frameCount)
    {
        _boolWrapper.Value = _switchOn;
        int i = 0;
        while (i < _frameCount)
        {
            i++;
            yield return new WaitForEndOfFrame();
        }
        _boolWrapper.Value = !_switchOn;
    }

    public void SetMeleeNum(int _ind)
    {
        anim.SetInteger("meleeNum", _ind);
    }

    public void SetDead(bool _isDead)
    {
        if (_isDead)
        {
            anim.SetInteger("deathNum", Random.Range(0, maxDeathAnims));
            dead = true;
        }
        else
        {
            dead = false;
            anim.Play("Idle", 0);
        }
    }
}
