﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemyAnimations))]
public class EnemyAnimationsEditor : Editor
{
    private EnemyAnimations source;
    private SerializedObject sourceRef;

    private SerializedProperty anim;
    //movement
    private SerializedProperty animSpeed;
    private SerializedProperty animGrounded;
    //health
    private SerializedProperty animHurt;
    private SerializedProperty animDead;

    private bool showMovement;
    private bool showHealth;

    private void OnEnable()
    {
        source = (EnemyAnimations)target;
        sourceRef = serializedObject;

        GetProperties();

    }

    public override void OnInspectorGUI()
    {
        SetProperties();

        sourceRef.ApplyModifiedProperties();
        Undo.RecordObject(source, "Modified " + source + " properties.");
    }

    void GetProperties()
    {
        anim = sourceRef.FindProperty("anim");
        //movement
        animSpeed = sourceRef.FindProperty("animSpeed");
        animGrounded = sourceRef.FindProperty("animGrounded");
        //health
        animHurt = sourceRef.FindProperty("animHurt");
        animDead = sourceRef.FindProperty("animDead");
    }

    void SetProperties()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("The Animator to sync");
        EditorGUILayout.PropertyField(anim);
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Type out the names of the controller parameters");
        showMovement = EditorGUILayout.Foldout(showMovement, "Movement");
        if (showMovement)
        {
            //movement
            EditorGUILayout.PropertyField(animSpeed);
            EditorGUILayout.PropertyField(animGrounded);
        }
        showHealth = EditorGUILayout.Foldout(showHealth, "Health");
        if (showHealth)
        {
            //health
            EditorGUILayout.PropertyField(animHurt);
            EditorGUILayout.PropertyField(animDead);
        }
  
        
    }

    
	
}
