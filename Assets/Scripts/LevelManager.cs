﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour 
{

    private const string CHECKPOINTKEY = "CheckPoint";
    private const string SCENEKEY = "Scene";
    private const string LOADSCENEKEY = "LoadScene";

    private int curCheckPoint;
    private string curScene;

    [SerializeField]
    private string loadingSceneName = "LoadingScreen";
    [SerializeField]
    private string gameOverSceneName = "GameOverScreen";

    void Start()
    {
        curCheckPoint = PlayerPrefs.GetInt(CHECKPOINTKEY);
    }

    #region SCENEMANAGEMENT

    public void LoadLevel(string _name)
    {
        SceneManager.LoadScene(_name);
    }

    public void LoadLevel(int _buildInd)
    {
        SceneManager.LoadScene(_buildInd);
    }

    public void LoadLevelWithLoadingScreen(string _levelName)
    {
        PlayerPrefs.SetString(LOADSCENEKEY, _levelName);
        SceneManager.LoadScene(loadingSceneName);
    }

    public string GetLoadingScreenNextLevelName()
    {
        return PlayerPrefs.GetString(LOADSCENEKEY);
    }

    public void ResetCurLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public string GetCurLevelName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public int GetCurLevelInd()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public string GetLevelName(int _buildInd)
    {
        return SceneManager.GetSceneByBuildIndex(_buildInd).name;
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    #endregion

    #region SAVINGCHECKPOINTS

    public void SaveCurCheckPoint(int _ind)
    {
        if (curCheckPoint == _ind)
            return;

        curCheckPoint = _ind;
        PlayerPrefs.SetInt(CHECKPOINTKEY, curCheckPoint);
        Debug.Log("Saving current checkpoint: " + curCheckPoint + " to disc.");
    }

    public void ResetCurCheckPoint()
    {
        curCheckPoint = 0;
        PlayerPrefs.SetInt(CHECKPOINTKEY, curCheckPoint);
    }

    public int GetCurSavedCheckPoint()
    {
        if (PlayerPrefs.HasKey(CHECKPOINTKEY))
        {
            return PlayerPrefs.GetInt(CHECKPOINTKEY);
        }
        else
        {
            Debug.Log("No checkpoint saved");
            return 0;
        }
    }

    #endregion

    #region SAVINGLEVELDATA

    public void SaveCurLevelName(string _levelName)
    {
        curScene = _levelName;
        PlayerPrefs.SetString(SCENEKEY, curScene);
    }

    public void EraseSavedLevelName()
    {
        PlayerPrefs.DeleteKey(SCENEKEY);
    }

    public string GetCurSavedLevelName()
    {
        if (PlayerPrefs.HasKey(SCENEKEY))
        {
            return PlayerPrefs.GetString(SCENEKEY);
        }
        else
        {
            Debug.Log("No Scene name data found. Returning null");
            return null;
        }
    }

    #endregion
}
