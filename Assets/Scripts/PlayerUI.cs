﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{

    [SerializeField]
    private Transform playerHpParent;
    [SerializeField]
    private GameObject playerHpPrefab;
    [SerializeField]
    private float curHealthScale = 1.1f;
    [SerializeField]
    private Text livesText;
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private Image dashCooldown;
    [SerializeField]
    private Image playerAvatar;

    private List <GameObject> hpSlots = new List<GameObject>();

    private int curHp = 0;
    private int curLives;
    Transform curSlot;

    public void SetPlayerAvater(Sprite _image)
    {
        if (playerAvatar)
            playerAvatar.sprite = _image;
    }

    public void AddHp(int _amount)
    {
        curHp += _amount;
        for (int i = 0; i < _amount; i++)
        {
            GameObject spawn = Instantiate(playerHpPrefab, playerHpParent);
            spawn.transform.localScale = Vector3.one;
            hpSlots.Add(spawn);
        }

        SetChosenHealthSlot(hpSlots.Count - 1);
    }

    public void RemoveHp(int _amount)
    {
        curHp -= _amount;
        for (int i = 0; i < _amount; i++)
        {
            int lastInd = hpSlots.Count - 1;
            Destroy(hpSlots[lastInd]);
            hpSlots.RemoveAt(lastInd);
        }
        SetChosenHealthSlot(hpSlots.Count - 1);
    }

    public void SetCooldownValue(float _value)
    {
        if (dashCooldown)
            dashCooldown.fillAmount = _value;
    }

    public void SetChosenHealthSlot(int _ind)
    {
        if (curSlot != null)
        {
            curSlot.localScale = Vector3.one;
        }

        if (_ind >=0)
            curSlot = hpSlots[_ind].transform;

        curSlot.localScale = Vector3.one * curHealthScale;
    }

    public void SetLives(int _amount)
    {
        livesText.text = _amount.ToString();
    }
	
    public void PauseMenuSetActive(bool _setActive)
    {
        pauseMenu.SetActive(_setActive);
    }
}
