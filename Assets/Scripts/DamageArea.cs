﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DamageArea : MonoBehaviour
{
    [System.Serializable]
    public class DamageZone
    {
        public bool opened = true;
        public bool areYouSureActive = false;
        public enum ZoneType { Box, Circle }
        public ZoneType zoneType = ZoneType.Box;
        public ZoneType lastType;
        public string zoneName = ZoneType.Box.ToString();
        public enum CenterType { Offset, Transform }
        public CenterType centerType;
        public Transform transPoint;
        public Vector2 center = Vector2.up;
        public Vector2 boxSize = Vector2.one;
        public float radius = 0.5f;
        public int damageAmount = 1;
        public float bounceForce;
        public enum BounceDirectionSetup { ClosestPoint, Override }
        public BounceDirectionSetup bounceSetup;
        public Vector2 bounceDir = Vector2.up;
        public Vector2 handlePoint;
        public Vector2 guiCenter;
        public Vector2 curCenter;
        public List<Collider2D> enteredCols = new List<Collider2D>();
        public List<Collider2D> damagedCols = new List<Collider2D>();
    }

    public List<DamageZone> zones = new List<DamageZone>();
    [SerializeField]
    private LayerMask damageMask;


    private void FixedUpdate()
    {
        DetectAreas();
    }

    void DetectAreas()
    {
        if (zones.Count < 1)
            return;

        foreach (var zone in zones)
        {
            //set offset
            if (zone.centerType == DamageZone.CenterType.Offset)
                zone.curCenter = (Vector2)transform.position + zone.center;
            else if (zone.centerType == DamageZone.CenterType.Transform)
                zone.curCenter = zone.transPoint.position;

            //get colliders
            if (zone.zoneType == DamageZone.ZoneType.Box)
                zone.enteredCols = Physics2D.OverlapBoxAll(zone.curCenter, zone.boxSize, 0, damageMask).ToList();
            else if (zone.zoneType == DamageZone.ZoneType.Circle)
                zone.enteredCols = Physics2D.OverlapCircleAll(zone.curCenter, zone.radius, damageMask).ToList();

            //Check for entered collisions
            if (zone.enteredCols.Count > 0)
            {
                //check if damaged yet
                foreach (var col in zone.enteredCols)
                {
                    if (!zone.damagedCols.Contains(col))//only damage once
                    {
                        Vector2 dir = zone.bounceDir;
                        if (zone.bounceSetup == DamageZone.BounceDirectionSetup.ClosestPoint)
                            dir = ((Vector2)col.bounds.ClosestPoint(zone.curCenter) - zone.curCenter).normalized;
                        DoDamage(col, zone.damageAmount,zone.bounceForce, dir);
                        zone.damagedCols.Add(col);
                    }
                       
                }
                
            }
            if (zone.damagedCols.Count > 0)
            {
                //check if damaged items left area
                for (int i = 0; i < zone.damagedCols.Count; i++)
                {
                    if (!zone.enteredCols.Contains(zone.damagedCols[i]))
                        zone.damagedCols.Remove(zone.damagedCols[i]);//Remove if left
                }
            }

        }
    }

    void DoDamage(Collider2D _col, int _damage, float _bounceForce, Vector2 _dir)
    {
        var obj = _col.gameObject;

        Player pl = obj.GetComponent<Player>();
        Enemy en = obj.GetComponent<Enemy>();
        if (pl)
        {
            pl.DamageHp(_damage, _bounceForce, _dir, true);
        }
        if (en)
        {
            en.DamageHp(_damage);
        }   
    }
}
