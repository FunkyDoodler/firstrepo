﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelFinish : MonoBehaviour 
{
    [SerializeField]
    private bool saveProgress;
    [SerializeField]
    public int sceneUnlocked;
    [SerializeField]
    public string nextSceneToPlay;
    [SerializeField]
    private bool freezeGame;
    [SerializeField]
    private float endTime;

    private LevelManager lm;
    private PlayerData pd;

    void Start()
    {
        //get components
        lm = GameManager.instance.GetLevelManager();
        pd = GameManager.instance.GetPlayerData();
    }

    void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.tag == "Player")
        {

            if (saveProgress)
            {
                pd.SaveCurProgress(sceneUnlocked);
            }

            //lm.LoadLevel(nextSceneToPlay);

            //using Game manager to "win" the level
            GameManager.instance.LevelWin(nextSceneToPlay, endTime, freezeGame);

        }
    }
}
