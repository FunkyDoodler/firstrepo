﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PlayerController))]
public class PlayerStompEnemies : MonoBehaviour
{

    [SerializeField]
    private int damage = 1;

    //instant kill
    [SerializeField]
    private bool instantKill;
    [SerializeField]
    private string instantKillTag;

    //detection
    [SerializeField]
    private LayerMask stompMask;
    [SerializeField]
    private Vector2 detectBoxSize = Vector2.one;
    [SerializeField]
    private Vector2 detectBoxCenter = Vector2.zero;

    //bouncing
    [SerializeField]
    private float bounceForce = 10;

    private Rigidbody2D rb;
    private PlayerController con;
    private Enemy curEnemy;
    private bool doDamage;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        con = GetComponent<PlayerController>();

    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        DoStompDetection();
	}

    void DoStompDetection()
    {
        if (rb.velocity.y > 0)//only do damage if moving downward
            return;

        Vector2 pos = transform.position;
        Collider2D[] cols = Physics2D.OverlapBoxAll(pos + detectBoxCenter, detectBoxSize, 0, stompMask);
        if (cols.Length > 0)
        {
            curEnemy = cols[0].GetComponent<Enemy>();
            if (curEnemy)
            {
                if (!doDamage)
                {
                    doDamage = true; //only damage enemy once
                    DoDamage(curEnemy);
                    Bounce();
                }
            }
        }
        else if (doDamage)
            doDamage = false; //reset once enemy leaves box
    }

    void DoDamage(Enemy _enemy)
    {
        if (instantKill && instantKillTag == _enemy.tag)
            _enemy.DamageHp(_enemy.GetCurHp());
        else
            _enemy.DamageHp(damage);
    }

    void Bounce()
    {
        con.Bounce(Vector2.up, bounceForce);
    }
}
