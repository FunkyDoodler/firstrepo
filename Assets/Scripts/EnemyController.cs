﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Enemy))]
public class EnemyController : MonoBehaviour 
{
    public enum EnemyMovementType {Static, Walking}

    [SerializeField]
    private bool faceRightAtStart = true;
    [SerializeField]
    private EnemyMovementType movementType = EnemyMovementType.Walking;
    [SerializeField]
    private float speed = 3;
    [SerializeField]
    private bool useBoundaries = true;
    public Vector2 leftPos;
    public Vector2 rightPos;

    [SerializeField]
    private bool flipOnCollision = true;
    [SerializeField]
    private LayerMask collisionMask;

    [SerializeField]
    private bool bounce;
    [SerializeField]
    private float bouncePower = 5;
    [SerializeField]
    private float bounceXAddedForce;
    [SerializeField]
    private float bounceDelay;
    private float bounceTimer;

    //Ground Detection
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    public Vector2 groundBoxSize = Vector2.zero;
    [SerializeField]
    public Vector2 groundBoxCenter = Vector2.zero;
    private bool grounded;
    public bool IsGrounded { get { return grounded; } }

    private EnemySoundFX soundFX;

    private bool facingRight = true;
    private bool stunned;

    private Rigidbody2D rb;
    private Enemy enemy;

    private bool pausePatrol;

    private float curSpeed;
    private Vector3 lastPos;
    private float lastYPos;
    private float curYPos;

    private Bounds bounds;

	// Use this for initialization
	void Start () 
	{

        InititalizeController();
        GetComponents();
        StartCoroutine(CalcSpeed());
    }
	
    void Update()
    {
        CheckGrounded();
    }

	// Update is called once per frame
	void FixedUpdate () 
	{
        if (pausePatrol || stunned || enemy.isDead())
            return;
        
        CheckBoundaries();
        MoveEnemy();

        if (bounce)
        {
            BounceEnemy();
        }

    }

    void GetComponents()
    {
        //get components
        rb = GetComponent<Rigidbody2D>();
        soundFX = GetComponent<EnemySoundFX>();
        enemy = GetComponent<Enemy>();
        bounds = GetComponent<Collider2D>().bounds;
    }

    void InititalizeController()
    {
        //make sure rotations are consistent
        transform.rotation = Quaternion.identity;
        if (!faceRightAtStart)
            FlipController();
    }

    public void StunEnemy(Vector2 _directionForce, float _stunTime)
    {
        StartCoroutine(StartStunEnemy(_directionForce, _stunTime));
    }

    IEnumerator StartStunEnemy(Vector2 _directionForce, float _stunTime)
    {
        stunned = true;

        //add force
        rb.AddForce(_directionForce, ForceMode2D.Impulse);

        float timer = 0;
        while (timer < _stunTime)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        stunned = false;
    }

    void MoveEnemy()
    {
        if (movementType == EnemyMovementType.Walking)
            //move enemy..forward
            transform.Translate(transform.right * speed * Time.deltaTime, Space.World);
        else if (movementType == EnemyMovementType.Static && bounce && !grounded && bounceXAddedForce > 0)
            transform.Translate(transform.right * bounceXAddedForce * Time.deltaTime, Space.World);
    }

    void CheckBoundaries()
    {
        if (!useBoundaries)
            return;

        //rotate enemy based in waypoint x position
        if ((transform.position.x - bounds.extents.x) <= leftPos.x && !facingRight)
        {
            FlipController();
        }
        else if ((transform.position.x + bounds.extents.x) >= rightPos.x && facingRight)
        {
            FlipController();
        }
    }

    void FlipController()
    {
        facingRight = !facingRight;
        transform.Rotate(0,180,0);
    }

    void CheckGrounded()
    {
        curYPos = Mathf.Round(transform.position.y * 100) / 100;

        if (curYPos != lastYPos) // only check ground if controller's y position changes..save some fps
        { 
            //ground detect
            grounded = Physics2D.OverlapBox((Vector2)transform.position + groundBoxCenter, groundBoxSize, 0, groundMask);
        }
        
        lastYPos = Mathf.Round(transform.position.y * 100) / 100; ;
    }

    void BounceEnemy()
    {
        if (grounded)
        {
            bounceTimer += Time.deltaTime;
            if (bounceTimer > bounceDelay)
            {
                rb.Sleep();

                rb.AddForce(transform.up * bouncePower, ForceMode2D.Impulse);

                //playsound
                if (soundFX)
                    soundFX.PlayAttackSound();

                bounceTimer = 0;
            }

        }    
    }

    void OnCollisionEnter2D(Collision2D _col)
    {
        if (flipOnCollision)
        {
            if (collisionMask == (collisionMask | (1<<_col.gameObject.layer)))
            {
                ContactPoint2D[] contacts = new ContactPoint2D[_col.contacts.Length];
                _col.GetContacts(contacts);
                ContactPoint2D point = contacts[0];

                float dir = (point.point.x - transform.position.x);

                if (dir < 0 && !facingRight)
                    FlipController();
                if (dir > 0 && facingRight)
                    FlipController();
            }
        }

    }

    public void PausePatrol(bool _pause)
    {
        pausePatrol = _pause;
    }

    IEnumerator CalcSpeed()
    {
        while (Application.isPlaying)
        {
            // Position at frame start
            lastPos = transform.position;
            // Wait till it the end of the frame
            yield return new WaitForFixedUpdate();
            // Calculate velocity: Velocity = DeltaPosition / DeltaTime
            curSpeed = ((lastPos - transform.position) / Time.fixedDeltaTime).magnitude;
        }
    }
}
