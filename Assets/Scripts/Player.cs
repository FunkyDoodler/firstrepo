﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [SerializeField]
    private int maxHp = 5;
    [SerializeField]
    private int maxLives = 3;
    [SerializeField]
    private Transform enemyTarget;
    [SerializeField]
    private List<GameObject> skins = new List<GameObject>();

    private int curHp;
    private int curLives;
    [SerializeField]
    private bool setSkin;
    [SerializeField]
    private int skinInd;
    private int curSkinInd;

    private bool dead;

    private PlayerUI ui;
    private PlayerData pd;
    private SpawnManager sm;
    private PlayerAnimations anim;
    private PlayerSoundFX soundFX;
    private PlayerController con;

    // Use this for initialization
    void Start()
    {
        //get components
        ui = GameManager.instance.GetPlayerUI();
        pd = GameManager.instance.GetPlayerData();
        sm = GameManager.instance.GetSpawnManager();
        con = GetComponent<PlayerController>();
        anim = GetComponent<PlayerAnimations>();

        curLives = maxLives;
        //save lives to disc
        pd.SetCurLives(curLives);

        //set player skin from disc
        if (setSkin)
            SetPlayerSkin(skinInd);
        else
            SetPlayerSkin(pd.GetCurPlayerSkin());

        //get sound
        soundFX = GetCurSkin().GetComponent<PlayerSoundFX>();

        //set playeravatar
        PlayerSkin skin = GetCurSkin().GetComponent<PlayerSkin>();
        if (skin)
            ui.SetPlayerAvater(skin.GetSkinAvater());

        SetHealthDefaults();
    }

    public void SetPlayerSkin(int _ind)
    {
        curSkinInd = _ind;
        pd.SetCurPlayerSkin(curSkinInd);

        foreach (var skin in skins)
        {
            if (skin != skins[curSkinInd])
            {
                skin.SetActive(false);
            }
            else
            {
                skin.SetActive(true);
                anim.SetAnimationController(skin.GetComponent<Animator>());
            }
        }
    }

    public void SetHealthDefaults()
    {
        curHp = maxHp;

        //set UI stats
        if (ui)
        {
            ui.AddHp(curHp);
            ui.SetLives(curLives);
        }

        dead = false;

        //update anim
        anim.SetDead(false);

    }

    public void AddHp(int _amount)
    {
        //only add health if not at max
        if (curHp < maxHp)
        {
            curHp += _amount;
            //update ui
            if (ui)
            {
                ui.AddHp(_amount);
            }
            
        }

    }

    public void DamageHp(int _damage)
    {
        ApplyDamage(_damage, 0, Vector2.zero, false);
    }

    public void DamageHp(int _damage, float _bounceForce, Vector2 _direction, bool _stunMovement)
    {
        ApplyDamage(_damage, _bounceForce, _direction, _stunMovement);
    }

    void ApplyDamage(int _damage, float _bounceForce, Vector2 _direction, bool _stunMovement)
    {
        if (dead) //dont apply damage if fade is in effect or dead
            return;

        curHp -= _damage;

        //bounce player
        if (_bounceForce > 0)
            BouncePlayer(_bounceForce, _direction);

        //stun player movement if grounded
        if (con.IsGrounded() && _stunMovement)
            con.StunDisableMovement(1);

        //play hurt sound
        if (soundFX)
            soundFX.PlayHurtSound();

        //sync anim for one frame
        if (anim)
            anim.BoolSwitch(anim.hurt, true, 1);

        //kill player if health at 0
        if (curHp <= 0)
        {
            curHp = 0;
            KillPlayer();
        }

        //update UI
        if (ui)
        {
            ui.RemoveHp(_damage);
        }

        Debug.Log("damaging player by " + _damage + " at " + _direction + " direction.");
        
    }

    void BouncePlayer(float _bounceForce, Vector2 _direction)
    {
        con.Bounce(_direction, _bounceForce);
    }

    public void KillPlayer()
    {
        if (dead)
            return;

        dead = true;
            
        //update anim
        if (anim)
            anim.SetDead(true);

        if (curHp > 0)
        {
            if (ui)//update UI
                ui.RemoveHp(curHp);

            curHp = 0;
        }
            
        curLives--;
        if (curLives > 0)
            Respawn();
        else
        {
            curLives = maxLives;
            Respawn();
        }
        
        Debug.Log(gameObject + " has died!");


        //play death sound
        if (soundFX)
            soundFX.PlayDeathSound();

    }

    void Respawn()
    {
        sm.RespawnPlayer();
    }

    public GameObject GetCurSkin()
    {
        return skins[curSkinInd];
    }

    public int GetCurSkinInd()
    {
        return curSkinInd;
    }

    public int GetCurSkinCount()
    {
        return skins.Count;
    }

    public int GetCurHp()
    {
        return curHp;
    }

    public Transform GetEnemyTarget()
    {
        return enemyTarget;
    }

    public bool IsDead()
    {
        return dead;
    }
}
