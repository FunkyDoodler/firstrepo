﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{

    [SerializeField]
    private float delay = 0.3f;
    [SerializeField]
    private float speed = 10;
    [SerializeField]
    private int damage = 10;
    [SerializeField]
    private Transform nozzle;
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private GameObject weaponDisappear;
    [SerializeField]
    private float disappearTime = 1;
    [SerializeField]
    private bool instantFireFirstShot = true;
    [SerializeField]
    private bool preventSpamming;
    [SerializeField]
    private bool recoilBeforeShoot;

    private Player player;
    private bool fire;
    private bool firedOnce;
    private MenuManager mm;
    private PlayerRecoil recoil;
    private GameObject curSkin;
    private WeaponSoundFX soundFX;
    private PlayerAnimations anim;

    private bool triggered;
    private bool weaponCool = true;


    private bool recoiled;

    // Use this for initialization
    void Start()
    {
        //getcomp
        mm = GameManager.instance.GetMenuManager();
        player = transform.root.GetComponent<Player>();
        recoil = player.GetCurSkin().GetComponent<PlayerRecoil>();
        anim = transform.root.GetComponent<PlayerAnimations>();

        //get skin on delay
        StartCoroutine(GetCurSkin());
    }

    IEnumerator GetCurSkin()
    {
        yield return new WaitForEndOfFrame();
        curSkin = player.GetCurSkin();

        //get sound FX
        soundFX = curSkin.GetComponent<WeaponSoundFX>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mm.isPaused())
            return;

        CheckFire();
    }

    void CheckFire()
    {
        if (Input.GetAxisRaw("XboxTriggerRight") > 0 || Input.GetButton("Fire1"))
        {
            if (!triggered)
            {
                triggered = true;
                StartCoroutine(FireButttonDown());
            }               
        }
        else if (!instantFireFirstShot )
        {
            if (firedOnce && triggered)
            {
                triggered = false;
                firedOnce = false;
            }
        }
        else if (triggered)
        {
            triggered = false;
        }

        if (fire)
        {
            StartCoroutine(FireWeapon());
        }

    }

    IEnumerator FireWeapon()
    {

        bool recoiling = false;

        if (instantFireFirstShot)
        {          
            if (preventSpamming)
            {
                if (weaponCool)
                {
                    SpawnBullet();
                    StartCoroutine(WeaponCoolDown());
                }
            }
            else
                SpawnBullet();              
        }

        float shootTimer = 0;
        while (triggered)
        {
            if (recoilBeforeShoot && !recoiling)
            {
                if (recoil)
                    recoil.Recoil();
                else if (anim)
                    //anim.SetAttackOnce();

                recoiling = true;
            }

            shootTimer += Time.deltaTime;
            if (shootTimer > delay)
            {
                SpawnBullet();
                firedOnce = true;
                shootTimer = 0;
                recoiling = false;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator FireButttonDown()
    {
        fire = true;
        yield return new WaitForEndOfFrame();
        fire = false;
    }

    IEnumerator WeaponDisappear()
    {
        weaponDisappear.SetActive(false);
        yield return new WaitForSeconds(disappearTime);
        weaponDisappear.SetActive(true);
    }

    IEnumerator WeaponCoolDown()
    {
        float coolTimer = 0;
        weaponCool = false;
        while (coolTimer < delay)
        {
            coolTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        weaponCool = true;
    }

    void SpawnBullet()
    {
        //vanish weapon if nessesary
        if (weaponDisappear)
            StartCoroutine(WeaponDisappear());

        //recoil
        if (!recoilBeforeShoot)
        {
            if (recoil)
                recoil.Recoil();
            ///else if (anim)
                //anim.SetAttackOnce();
        }

        var bullet = Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
        PlayerBullet bul = bullet.GetComponent<PlayerBullet>();

        bul.SetParams(damage, speed);

        //play Soundfx
        soundFX.PlayFireSound();
    }
}
