﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerData : MonoBehaviour 
{

    private const string CHARKEY = "CharKey";
    private const string COINSKEY = "CoinsKey";
    private const string LIVESKEY = "LivesKey";
    private const string HEALTHKEY = "HealthKey";
    private const string LEVELPROGRESSKEY = "LevelProgress";


    private int curSkinInd;
    private int curCoins;
    [SerializeField]
    private int defLives = 3;
    private int curLives;
    [SerializeField]
    private int defHealth;
    private int curHealth;
    private int curLevelProgress;

    void Start()
    {
        curLevelProgress = GetCurProgress();
    }

    #region SKIN

    public void SetCurPlayerSkin(int _curInd)
    {
        curSkinInd = _curInd;
        PlayerPrefs.SetInt(CHARKEY, curSkinInd);
    }

    public void ResetPlayerSkin()
    {
        curSkinInd = 0;
        PlayerPrefs.SetInt(CHARKEY, curSkinInd);
    }

    public int GetCurPlayerSkin()
    {
        if (PlayerPrefs.HasKey(CHARKEY))
        {
            return PlayerPrefs.GetInt(CHARKEY);           
        }
        else
        {
            return 0;
        }
    }

    #endregion

    #region COINS

    public void SetCurCoins(int _amount)
    {
        curCoins = _amount;
        PlayerPrefs.SetInt(COINSKEY, curCoins);
    }

    public int GetCurCoins()
    {
        if (PlayerPrefs.HasKey(COINSKEY))
        {
            return PlayerPrefs.GetInt(COINSKEY);
        }
        else
        {
            return 0;
        }
    }

    public void ResetCoins()
    {
        curSkinInd = 0;
        PlayerPrefs.SetInt(COINSKEY, curCoins);
    }

    #endregion

    #region LIVES

    public void SetCurLives(int _curLives)
    {
        curLives = _curLives;
        PlayerPrefs.SetInt(LIVESKEY, curLives);
    }

    public void ResetLives()
    {
        curLives = defLives;
        PlayerPrefs.SetInt(LIVESKEY, curLives);
    }

    public int GetCurLives()
    {
        if (PlayerPrefs.HasKey(LIVESKEY))
        {
            return PlayerPrefs.GetInt(LIVESKEY);
        }
        else
        {
            return 0;
        }
    }

    #endregion

    #region HEALTH

    public void SetCurHealth(int _curHealth)
    {
        curHealth = _curHealth;
        PlayerPrefs.SetInt(HEALTHKEY, curHealth);
    }

    public void ResetHealth()
    {
        curHealth = defHealth;
        PlayerPrefs.SetInt(HEALTHKEY, curHealth);
    }

    public int GetCurHealth()
    {
        if (PlayerPrefs.HasKey(HEALTHKEY))
        {
            return PlayerPrefs.GetInt(HEALTHKEY);
        }
        else
        {
            return 0;
        }
    }

    #endregion

    #region LEVELPROGRESS

    public void SaveCurProgress(int _buildInd)
    {
        if (_buildInd > curLevelProgress)
        {
            Debug.Log("Saving Level Progress to Disc. Build index " + _buildInd + " unlocked.");
            curLevelProgress = _buildInd;
            PlayerPrefs.SetInt(LEVELPROGRESSKEY, curLevelProgress);
        }
        else
            Debug.Log("Level progress already unlocked to this level"); 
    }

    public int GetCurProgress()
    {
        if (PlayerPrefs.HasKey(LEVELPROGRESSKEY))
        {
            return PlayerPrefs.GetInt(LEVELPROGRESSKEY);
        }
        else
        {
            Debug.Log("No Progress Saved");
            return 0;
        }
    }

    public void ResetProgress()
    {
        curLevelProgress = 0;
        PlayerPrefs.SetInt(LEVELPROGRESSKEY, curLevelProgress);
    }

    #endregion

}